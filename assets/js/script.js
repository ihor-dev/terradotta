/**
 * Number.prototype.format(n, x, s, c)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace(',', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || '.'));
};

function radio_select(){
	$("input[type=radio]").removeAttr("selected");
	$("input[type=radio]:checked").attr("selected",true);
}

function count_slides(){
	var i = 0;
	$("section.slide").each(function(){		
		i = i+1;		
	});
	return i;
}

function pr_next_fields(){	
	slide   = $("#slideNum").val();
	
	$("#slideNext").val(parseInt(slide) + 1);
	$("#slidePrev").val(parseInt(slide) - 1);


}

//show/hide nav buttons
function init_buttons(slide){
	var count = count_slides();
	$("#slideNum").val(slide);

	if(slide == 1){
		$(".prev").hide();
		$(".next").show();
		$(".start").hide();
		$(".next").html('Next');
	}else if(slide == count){
		$(".next").hide();		
		$(".prev").show();
		$(".start").show();
	}else{
		$(".prev").show();
		$(".next").show();
		$(".start").hide();
	}
	if(slide == count-1){
		$(".next").html('Generate a preview');
	}

	pr_next_fields();

}

function progress(){
	var count = count_slides();
	var slide = $("#slideNum").val();
	var percent = 100/ count * slide;
	
	$(".progress-bar").animate({"width":percent+"%"},300);
}

//display slide section
function show_slide(n){
	$("section.slide, label.title").hide().animate({"opacity":"0"},200);
	$("section.slide_"+n+", label.title_"+n).show().animate({"opacity":"1"},300);
}

function valid_email_address(email)
{
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(email);
}

function checkField(el){
	if(el == '#Email'){
		if($(el).val() == "" || $(el).val() == undefined || !valid_email_address($(el).val()) ) {
			$(el).css({"border":"solid 1px red"});
			return 1;
		}else{
			$(el).css({"border":"solid 1px #999"});
			return 0;
		}
	}

	if($(el).val() == "" || $(el).val() == undefined ) {
		$(el).css({"border":"solid 1px red"});
		return 1;
	}else{
		$(el).css({"border":"solid 1px #999"});
		return 0;
	}
}

function validate(){
	var slide = $("#slideNum").val();
	var valid = $("#valid");	
	var error = 0;

	if(slide == '1'){		
		//validate fields on slide 1
		error += checkField("#firstName");
		error += checkField("#lastName");
		error += checkField("#Email");
		error += checkField("#companyName");
		error += checkField("#title");
		error += checkField("#soft_using");		
		
	}else if(slide == '2'){
		//validate fields on slide 2		
		error += checkField(".paymentIntegration[selected=selected]");				
		error += checkField(".softNumber");
	}
	if(error > 0){
		valid.val(0);
	}else{
		valid.val(1);
	}
}

//do some actions on button click
function init_slide(num){
	init_buttons(num);
	show_slide(num);
	progress();

}

function calculate(){
	this.fname = $("#firstName").val();
	this.lname = $("#lastName").val();
	this.email = $("#Email").val();
	this.companyName = $("#companyName").val();
	this.Title = $("#title").val();
	this.softUsing = $("#soft_using").val();
	this.Location = $(".location[selected=selected]").val();               	 	 // yes\no
	this.softNumber = parseInt($(".softNumber").val());                          // int

	this.license = this.softNumber > 580 ? 'unlimited' : 'service'; 	   		 // unlimited\service
	this.licensePeriod = '10_year';     							 	   		 // up_front\10_year
	this.softLocation = 'terra';        							       		 // terra\local		
	
	
	this.paymentIntegration = $(".paymentIntegration[selected=selected]").val(); // yes\no	
	this.advantage = $(".advantage[selected=selected]").val(); 					 // 10h\10h4m\20h\no		
	
	this.calcType = 'domestic';

	this.comment = $('textarea[name=comment]').val();

	this.doOnInit =  function(){
		
		var slide = $("#slideNum").val();

		if(slide=='1'){
			$("#totalCostForm").hide();
		}else{
			$("#totalCostForm").show();
		}
		this.fieldDo();
	}

	this.fieldDo = function(){		

		$("tr:has(td.hs)").hide();
		$("tr:has(td.hs)").find('td').attr('data-val',0);

		if(this.license == 'unlimited'){
			$(".maintenance, .LP").show(400);
			
			$("tr:has(td.applications-1)").hide();
			$("tr:has(td.applications-1)").find('td').attr('data-val',0);	

			$("tr:has(td.st)").show();

			
		}else {
			$(".maintenance, .LP").hide();
			$("tr:has(td.hs)").hide();
			$("tr:has(td.hs)").find('td').attr('data-val',0);			
			$("tr:has(td.st)").hide();
			$("tr:has(td.st)").find('td').attr('data-val',0);

			if(this.softUsing != 'Agreements'){
				$("tr:has(td.applications-1)").show();
			}							
		}

		if(this.softUsing == 'Agreements'){
			$("tr:has(td.license-1)").hide();
			$("tr:has(td.license-1)").find('td').attr('data-val',0);

			//hide gateway
			$("#pay-gateway").hide();
		}else{
			$("tr:has(td.license-1)").show();

			//show gateway			
			$("#pay-gateway").show();
		}


		
		$(".title_3").html("Quote for your Terra Dotta Software for "+this.softUsing);
	}


	this.setTotalcost = function(val1, val2){
		start1 = $(".totalCost1").attr('data-cost');
		start2 = $(".totalCost2").attr('data-cost');

		function labelUpdate1(p){ $(".totalCost1").html('$' + Math.round(p).format(2, 3, ',', '.'))}
		$({price:start1}).animate({price:val1},{ step:labelUpdate1, duration: 1200, easing: 'easeOutQuint'});
		
		function labelUpdate2(p){ $(".totalCost2").html('$' + Math.round(p).format(2, 3, ',', '.'))}
		$({price:start2}).animate({price:val2},{ step:labelUpdate2, duration: 1200, easing: 'easeOutQuint'});


		$(".totalCost1").attr('data-cost', val1);
		$(".totalCost2").attr('data-cost', val2);
	}

	this.setTablevars = function(el, val1, val2){
		
		if(el == 'license'){
			for(var i=1; i<=2; i++){
				if(i==1){
					$(".license-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));
				}else{
					$(".license-"+i).html("$"+parseFloat(val2).format(2, 3, ',', '.')).attr('data-val',val2.toFixed(2));
				}
			}
		}else if(el == 'applications'){
			var res = val1;
			for(var i=1; i<=2; i++){					
				$(".applications-"+i).html("$"+parseFloat(res).format(2, 3, ',', '.')).attr('data-val',res.toFixed(2));
			}
		}else if(el == 'di'){
			$(".di").show();
			for(var i=1; i<=2; i++){
				if(i==1){
					$(".di-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));				
				}else{
					$(".di-"+i).html("$"+parseFloat(0).format(2, 3, ',', '.')).attr('data-val',0);				
				}
			}
		}else if(el == 'dtr'){
			$(".dtr").show();
			for(var i=1; i<=2; i++){
				if(i==1){
					$(".dtr-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));				
				}else{
					$(".dtr-"+i).html("$"+parseFloat(0).format(2, 3, ',', '.')).attr('data-val',0);				
				}
			}
		}else if(el == 'pay'){			
			for(var i=1; i<=2; i++){
				$(".pay").show();
				if(i>1){
					$(".pay-"+i).html("$"+parseFloat(val2).format(2, 3, ',', '.')).attr('data-val',val2.toFixed(2));
				}else{
					$(".pay-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));
				}				
			}
		}else if(el == 'gri'){
			$(".gri").show();
			for(var i=1; i<=2; i++){
				if(i==1){
					$(".gri-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));				
				}else{
					$(".gri-"+i).html("$"+parseFloat(0).format(2, 3, ',', '.')).attr('data-val',0);				
				}
			}
		}else if(el == 'advantage'){
			$(".advantageT").show();
			for(var i=1; i<=2; i++){
				if(i==1){
					$(".advantage-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));			
				}else{
					$(".advantage-"+i).html("$"+parseFloat(0).format(2, 3, ',', '.')).attr('data-val',0);			
				}
			}
		}else if(el == 'train'){
			$(".train").show();
			for(var i=1; i<=2; i++){
				if(i==1){
					$(".train-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));				
				}else{
					$(".train-"+i).html("$"+parseFloat(0).format(2, 3, ',', '.')).attr('data-val',0);				
				}
			}
		}


		else if(el == 'grTotalServ'){
			
			for(var i=1; i<=2; i++){
				var res = parseFloat($(".license-"+i).attr('data-val'));				
				res += parseFloat($(".applications-"+i).attr('data-val'));
				res += parseFloat($(".impl-"+i).attr('data-val'));
				res += parseFloat($(".di-"+i).attr('data-val'));
				res += parseFloat($(".dtr-"+i).attr('data-val'));
				res += parseFloat($(".pay-"+i).attr('data-val'));				
				res += parseFloat($(".advantage-"+i).attr('data-val'));
				res += parseFloat($(".train-"+i).attr('data-val'));
				
				
				$(".grTotal-"+i).html("$"+parseFloat(res).format(2, 3, ',', '.'));

				if(i==1){
					var res5 = ((res / 100) * 5) + res;				
					$(".contingency1").html("$"+parseFloat(res5).format(2, 3, ',', '.'));
				}
			}
		}else if(el == 'hostServ'){
			for(var i=1; i<=2; i++){				
				$(".hostServ-"+i).html("$"+parseFloat(val1).format(2, 3, ',', '.')).attr('data-val',val1.toFixed(2));							
			}
		}else if(el == 'subTotal'){
			for(var i=1; i<=2; i++){
				host = parseFloat($(".hostServ-"+i).attr('data-val')) ? parseFloat($(".hostServ-"+i).attr('data-val')) : 0;
				var res = parseFloat($(".license-"+i).attr('data-val'));				
				res += parseFloat($(".applications-"+i).attr('data-val'));
				res += parseFloat($(".impl-"+i).attr('data-val'));
				res += host;
				res += parseFloat($(".di-"+i).attr('data-val'));
				res += parseFloat($(".dtr-"+i).attr('data-val'));
				res += parseFloat($(".pay-"+i).attr('data-val'));
				res += parseFloat($(".gri-"+i).attr('data-val'));
				res += parseFloat($(".advantage-"+i).attr('data-val'));
				res += parseFloat($(".train-"+i).attr('data-val'));
				//$(".subtotal-"+i).html("$"+parseFloat(res).toFixed(2)).attr('data-val',res.toFixed(2));

				if(i==1){					
					$(".grTotal-"+i).html("$"+parseFloat(res).format(2, 3, ',', '.'));
					
					var res5 = ((res / 100) * 5) + res;				
					$(".contingency1").html("$"+parseFloat(res5).format(2, 3, ',', '.'));
				}else{					
					$(".grTotal-"+i).html("$"+parseFloat(res).format(2, 3, ',', '.'));
				}
			}
		}else if(el == 'hardLocal'){
			$(".cfLicense-1").html("$"+parseFloat(val1[0]).format(2, 3, ',', '.')).attr('data-val',val1[0].toFixed(2));						
			$(".dbServer-1").html("$"+parseFloat(val1[1]).format(2, 3, ',', '.')).attr('data-val',val1[1].toFixed(2));						
			$(".apServer-1").html("$"+parseFloat(val1[2]).format(2, 3, ',', '.')).attr('data-val',val1[2].toFixed(2));		
			res = parseFloat(val1[0]+val1[1]+val1[2]).format(2, 3, ',', '.');				
			$(".tHardCost-1").html("$"+res).attr('data-val',res);
		
		}

	}

	this.domestic_calc = function(){
		var totalCost1 = 0;
		var totalCost2 = 0;

		//Agreements
		if(this.softUsing == 'Agreements'){			
			
			totalCost1 += 6500 + 4800;
			totalCost2 += 4800;
			
			this.setTablevars('applications', 4800, 0);

		}else{
		//Other

			totalCost1 += 6500;

			if(this.license == 'service'){
				totalCost1 += 2500 + (this.softNumber * 30);
				totalCost2 += 2500 + (this.softNumber * 30) ;

				this.setTablevars('license', 2500,2500);
				this.setTablevars('applications', this.softNumber * 30);

			}else if(this.license == 'unlimited'){
				totalCost1 += 9800;
				totalCost2 += 9800;

				if(this.softLocation == 'terra'){
					totalCost1 += 4700;
					totalCost2 += 4700;				
					
					this.setTablevars('hostServ',4700);
				}

				if(this.licensePeriod == '10_year'){
					totalCost1 += 5400;
					totalCost2 += 5400;
					
					this.setTablevars('license', 5400 + 9800, 5400 + 9800);
				}
			}
		}

		


		var totalAdServ1 = 0;
		var totalAdServ2 = 0;

		//hide all addServices
		$(".train, .advantageT, .gri, .pay, .dtr, .di").find('td:not(.h)').html("");
		$(".train, .advantageT, .gri, .pay, .dtr, .di").find('td:not(.h)').attr('data-val', 0);
		$(".train, .advantageT, .gri, .pay, .dtr, .di").hide();


		if(this.paymentIntegration == 'yes'){
			totalCost1 += 2000;
			totalCost2 += 500;

			totalAdServ1 += 2000;
			totalAdServ2 += 500;

			this.setTablevars('pay', 2000,500);
		}		

		if(this.advantage == '10h'){
			totalCost1 += 5400;
			totalAdServ1 += 5400;
			this.setTablevars('advantage', 5400);
		}else if(this.advantage == '10h4m'){
			totalCost1 += 1600;
			totalAdServ1 += 1600;
			this.setTablevars('advantage', 1600);
		}else if(this.advantage == '20h'){
			totalCost1 += 9600;
			totalAdServ1 += 9600;
			this.setTablevars('advantage', 9600);
		}	
		

		if(this.license == 'unlimited'){
			this.setTablevars('subTotal',0,0);
			this.setTablevars('grTotalUnlim',0,0);
		}else if( this.license == 'service'){
			this.setTablevars('grTotalServ',0,0);
		}

		this.setTotalcost(totalCost1, totalCost2);
	}


	this.calc = function(){
		this.domestic_calc();
	}

	this.createPDF = function(){
		var data = {
			"calcType" : this.calcType,
			"license" : this.license,
			"softNumber" : this.softNumber,
			"softLocation" : this.softLocation,
			"licensePeriod" : this.licensePeriod,
			"softUsing" : this.softUsing,
			"fname" : this.fname,
			"lname" : this.lname,
			"title" : this.Title,
			"company" : this.companyName,
			"paymentIntegration" : this.paymentIntegration,						
			"email" : this.email,
			"comment": this.comment
		}

		this.sendAjax(data);
	}
	this.sendAjax = function(data){
		$.ajax({			
			url: "pdf.php",
			type: 'POST',
	        async: true,
	        data: data,
	        dataType: "json",
	        cache: false,
		})
		.done(function( msg ) {
			$(".alert").hide();
			$(".alert").removeClass('hide');
			$(".alert").show(600);
		});
	}

	this.calc();
	this.doOnInit();

}


$(document).ready(function(){
	new calculate();
		$(".next").popover({
			title: "Validation error:",
			content: 'Complete all fields, please!',
			placement: 'top',
			trigger: 'manual'	
		});
	$(".next").click(function(){
		validate();		
		if($("#valid").val() == 0){
			$(".next").popover('show');

			setTimeout(function(){
				$(".next").popover('hide');	
			},3000);
			return false;
		}
		init_slide($("#slideNext").val());
		var a = new calculate();
		a.doOnInit();
	});

	$(".prev").click(function(){		
		init_slide($("#slidePrev").val());
		var a = new calculate();
		a.doOnInit();
	});

	$(".start").click(function(){		
		var a = new calculate();
		a.createPDF();

	});

	$("input").change(function(){
		radio_select();
		pr_next_fields();
		$(this).css({'border': 'solid 1px #999'});
		
	});

	$("input").change(function(){
		var a = new calculate();
		a.fieldDo();
	});
	

	init_slide(1);

	radio_select();


	$(".help").popover({"trigger":"hover"});

});