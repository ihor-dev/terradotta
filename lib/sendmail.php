<?php
require_once('lib/mailer.php');

$to = $_POST['email'];
$from = 'Terra Dotta';
$from_mail = 'info@terradotta.com';
$html = "Welcome! In the attached file there is a price table of your calculations";

Send_mail::from($from_mail, $from)  // Sender's name and address. // Second argument is not a mandatory.
 
         ->to($to, 'BITCRACK CYBER SECURITY')  /* Sender's name and address (can be a list of addresses).*/
         
         ->files($filename)  //attach files

         // Subject.
         ->subject('Terra Dotta Price PDF')          
               
         // Body.
         ->message($html)                  

         // Notifier. Default - false.
         ->notify(true)                                          
               
         // Message priority. True, if higher priority. Default - false.
         ->important(true)                    
               
         // Charset ( default - utf-8)                                               
         ->charset('utf-8')                    
               
         // set_time_limit (default == 30с.)
         ->time_limit(30)                      
               
         // Message type (default text/plain)
         ->content_type(Send_mail::CONTENT_TYPE_HTML)  
               
         // Message type (default - 'quoted-printable').
         ->content_encoding(Send_mail::CONTENT_ENCODING_QUOTED_PRINTTABLE)  
               
         // Function for sending.     
         ->send();
?>