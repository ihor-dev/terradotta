<?
	//print_r($_POST);
	require_once(__DIR__.'/lib/tcpdf/tcpdf.php');	

	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('TerraDotta');
	$pdf->SetTitle('Terra Dotta Price');
	$pdf->SetSubject('pdf');
	$pdf->SetKeywords('price, terradotta');

	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// set default monospaced font
	//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetFont('proximanova', '', 8);
	

	// add a page
	$pdf->AddPage();

	// create some HTML content
	$style = '
	<style>
	td{
		border: solid 1px #ccc;
		font-size: 8px;
		text-align: center;
	}

	h1{
		
		font-size: 13px;
		text-align: center;
	}
	.fCol{
		text-align: left;
		padding: 21px;
	}
	.subtable {
		width: 50%;
		padding-bottom: 144px;
		position: relative;
		display: block;
	}
	.text-info {
		color: #318DBC;
	}
	.br {
		padding: 144px;
	}
	.preparedFor {
		font-size: 10px;
		text-align: center;
	}

	.comment {
		font-size: 10px;
	}

	</style>';

	$html = '
	<img src="./assets/img/header.png" style="text-align:center">
	<p class="preparedFor">Estimate prepared for:<br><br>
	{company}<br>
	{fname} {lname},  {title}<br><br>
	Product<br><br>
	Terra Dotta Software for {softUsing}</p>
	
	<table class="responsive" style="padding: 2px 2px 2px 2px; width:100%;">
		<thead style="font-weight:bold;">
			<tr>
				<td class="fCol" width="33%">Category Subtotals</td>
				<td>Year 1</td>
				<td>Subsequent Years</td>

			</tr>
		</thead>
		<tbody>';
if($_POST['softUsing'] !== 'Agreements'){
	$html .='
			<tr>
				<td class="fCol" width="33%">Annual Service Fee</td>
				<td>{lic1}</td>
				<td>{lic2}</td>
				 
			</tr>';
}


if($_POST['license'] == 'service'){	
	$html .='
			<tr>
				<td class="fCol" width="33%">Application Fees (Based on {feesNubmer} Applications)</td>
				<td>{af1}</td>
				<td>{af2}</td>
 
			</tr>';
}

	$html .='
			<tr>
				<td class="fCol" width="33%">Implementation Services</td>
				<td>$6,500.00</td>
				<td>$0.00</td>
 
			</tr>';


if($_POST['paymentIntegration'] !=='no'){
	$html .='
			<tr>
				<td class="fCol">Credit Card Integration</td>
				<td>{pay1}</td>
				<td>{pay2}</td>
 
			</tr>
	';
}



if($_POST['license'] == 'unlimited' && $_POST['softLocation'] == 'local'){
	$html .='		
			<tr hardstyle >
				<td class="fCol" >Cold Fusion License</td>
				<td>{cfl1}</td>
				<td></td>
 				
			</tr>
			<tr hardstyle >
				<td class="fCol" >Databdbse Server</td>
				<td>{dbs1}</td>
				<td></td>
 				
			</tr>
			<tr hardstyle >
				<td class="fCol">Application Server</td>
				<td>{apps1}</td>
				<td></td>
				
			</tr>
			<tr hardstyle >
				<td class="fCol">Total Hardware Costs</td>
				<td>{thc1}</td>
				<td></td>

			</tr>';
}			
	$html .='
			<tr>
				<td class="fCol">Grand Total</td>
				<td>{grt1}</td>
				<td>{grt2}</td>

			</tr>
		</tbody>

	</table>
	
';

if(isset($_POST['comment']) && $_POST['comment'] != ""){
	$html .='		
	<div class="br">&nbsp;</div>
		<p class="comment"><b class="text-info">Additional comment:</b> <i>"{comment}"</i></p>
		
	';
}

$html .='
	<p class="subtable"><b class="text-info">Implementation Services</b> include project kickoff meetings, a one-week workshop, a standard integration, 10 hours/month for 4 months of training and assistance to develop content and processes.</p>
	<div class="br">&nbsp;</div>

	<p style="text-align:center;width:100%;">TERRADOTTA.COM</p>
	<p style="text-align:center;width:100%;">Terra Dotta, LLC 101 Conner Drive, Suite #301, Chapel Hill, NC 27514</p>
';
			
		$p = $_POST;
		$ct = $_POST['calcType'];
		$totalAdS1 = 0;
		$totalAdS2 = 0;
		$tArr = array();

		$html = str_replace("{fname}", $_POST['fname'], $html);
		$html = str_replace("{lname}", $_POST['lname'], $html);
		$html = str_replace("{title}", $_POST['title'], $html);
		$html = str_replace("{company}", $_POST['company'], $html);
		$html = str_replace("{softUsing}", $_POST['softUsing'], $html);
		$html = str_replace("{feesNubmer}", $_POST['softNumber'], $html);
		
		$html = str_replace("{endDate}", date('m.d.Y', strtotime('+6 months')), $html);

		$html = str_replace("{comment}", $_POST['comment'], $html);

		$totalAdS1 += 6500;
		
		function setTableVar($name,$val1=null, $val2=null){
			global $html;
			for($i=1; $i<=2; $i++){
				if($i == 1){
					$html = str_replace("{".$name.$i."}", "$".number_format($val1,2), $html);
				}else{
					if($val2 !== null){
						$html = str_replace("{".$name.$i."}", "$".number_format($val2,2), $html);
					}else{
						$html = str_replace("{".$name.$i."}", "$".number_format($val1,2), $html);
					}
				}
			}
		}

		if($p['paymentIntegration'] == "yes"){
			$totalAdS1 += 2000;
			$totalAdS2 += 500;
			setTableVar('pay',2000, 500);
		}
	

		for($i=1; $i<=5; $i++){					
			if($i>1){
				$var = $totalAdS2;
			}else{
				$var = $totalAdS1;
			}
			$html = str_replace("{as$i}", "$".$var.".00", $html);
			$tArr['as'][$i] = $var;
		}

		//=============================================

		if($_POST['softUsing']=='Agreements'){
			setTableVar('af',4800, 4800);
			$tArr['lic'][1] = 4800;
			$tArr['lic'][2] = 4800;
		}else{

			if($_POST['license'] == 'service'){			
				for($i=1; $i<=2; $i++){
						$var = 2500;
						$vArr = 2500;
						$html = str_replace("{lic$i}", "$".number_format($var,2), $html);
						$tArr['lic'][$i] = $vArr;
				}		
				for($i=1; $i<=5; $i++){
					$var = 30 * $_POST['softNumber'];
					
					$html = str_replace("{af$i}", "$".number_format($var,2), $html);
					$tArr['af'][$i] = $var;
				}				
			}else if($_POST['license'] == 'unlimited'){
				//UNLIMITED
				if($_POST['softLocation'] == 'terra'){
					for($i=1; $i<=2; $i++){
						$var = 4700;
						$tArr['hs'][$i] = $var;
						$html = str_replace("{hs$i}", "$".number_format($var,2), $html);
						$tArr['thc'] = 0;
					}
				}
				if($_POST['licensePeriod'] == '10_year'){
					for($i=1; $i<=2; $i++){
						$res = 5400 + 9800;
						$tArr['lic'][$i] = $res;
						$html = str_replace("{lic$i}", "$".number_format($res,2), $html);
					}
				}

				//subtotal
				for($i=1; $i<=2; $i++){
					$sbt = $tArr['lic'][$i] + $tArr['as'][$i] + $tArr['hs'][$i];
					$html = str_replace("{sbt$i}", "$".number_format($sbt,2), $html);
					$tArr['sbt'][$i] = $sbt;
				}
			}
		}
		

		//grand totals
		if($_POST['license'] == 'service'){	
			for($i=1; $i<=2; $i++){	
				$res = $tArr['lic'][$i] + $tArr['af'][$i] + $tArr['as'][$i];
				$html = str_replace("{grt$i}", "$".number_format($res,2), $html);
				if($i==1){
					$res += $res * 0.05;
					$html = str_replace("{cntg$i}", "$".number_format($res,2), $html);
					
				}
			}
		}else {
			for($i=1; $i<=2; $i++){	

				if($i==1){
					$res = $tArr['sbt'][$i] + $tArr['thc'];
					$resCntg = $res + ($res * 0.05);
					$html = str_replace("{cntg$i}", "$".number_format($resCntg,2), $html);
				}else{
					$res = $tArr['sbt'][$i];
				}
				$html = str_replace("{grt$i}", "$".number_format($res,2), $html);
			}
		}

	$out = $style . $html;
	//echo $out;
	
	// output the HTML content
	$pdf->writeHTML($out, true, false, true, false, '');
	$path = __DIR__."/pdf/";
	$filename = $path . str_replace(" ", "_", $_POST['company']) .'.pdf';
	//$filename = $path . '123.pdf';
	
	
	$pdf->Output($filename, 'F');

	//send email width PDF
	require_once(__DIR__.'/lib/sendmail.php');

	//remove pdf
	//unlink($filename);
	echo '1';

?>